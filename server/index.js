const express = require("express");
const http = require("http");
const cors = require("cors");
const dotenv = require("dotenv");

const cookieParser = require("cookie-parser");

const db = require("./src/Models");
const userRoutes = require("./src/Router/userRouter");

//setting up your port
dotenv.config();
const PORT = process.env.PORT_API;

//assigning the variable app to express
const app = express();

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//synchronizing the database and forcing it to false so we dont lose data
db.sequelize.sync({ force: true }).then(() => {
  console.log("db has been re sync");
});

app.use(cors());

const server = http.createServer(app);

//routes for the user API
app.use("/api/users", userRoutes);

//listening to server connection
server.listen(PORT, () => console.log(`Server is connected on ${PORT}`));
