//importing modules
const { Sequelize, DataTypes } = require("sequelize");

//database name is discover
const sequelize = new Sequelize("apempaiq", "postgres", "pass", {
  host: "postgres",
  dialect: "postgres",
});

//checking if connection is done
sequelize
  .authenticate()
  .then(() => {
    console.log(`Database connected to discover`);
  })
  .catch((err) => {
    console.log(err);
  });

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

//connecting to model
db.users = require("./userModel")(sequelize, DataTypes);

//exporting the module
module.exports = db;
