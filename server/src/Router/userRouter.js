const express = require("express");

const authController = require("../Controllers/authController");
const userController = require("../Controllers/userController");
const { checkJWT } = require("../Middleware/security");

const { signup, login } = authController;
const { getUsers, deleteUser, updateUser, getMyUsers } = userController;
const userAuth = require("../Middleware/userAuth");

const router = express.Router();

//signup endpoint
router.post("/signup", userAuth.saveUser, signup);

//login route
router.post("/login", login);

router.get("/", checkJWT, getUsers);
router.get("/:id", checkJWT, getMyUsers);
router.put("/:id", checkJWT, updateUser);
router.delete("/:id", checkJWT, deleteUser);

module.exports = router;
