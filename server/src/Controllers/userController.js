const db = require("../Models");
const { Op } = require("sequelize");

const User = db.users;

const getUsers = async (request, response) => {
  const allData = await User.findAll();
  response.status(200).send(allData);
};

const getMyUsers = async (request, response) => {
  const id = request.params.id;
  const allData = await User.findAll({
    where: {
      [Op.not]: [{ id: id }],
    },
  });
  response.status(200).send(allData);
};

const updateUser = async (request, response) => {
  const id = parseInt(request.params.id);
  const { nom, email } = request.body;

  const user = await User.update(
    { userName: nom, email: email },
    {
      where: {
        id: id,
      },
    }
  );
  response.status(200).send(`User modified : ${user}`);
};

const deleteUser = async (request, response) => {
  const id = parseInt(request.params.id);

  await User.destroy({
    where: {
      id: id,
    },
  });

  response.status(200).send(`User deleted with ID: ${id}`);
};

module.exports = {
  updateUser,
  deleteUser,
  getUsers,
  getMyUsers,
};
